import { html } from 'lit-html';
import { withShadow } from '@/decorators';
import {
	faAnkh, faBan, faCog, faFlag, faSkating, faSkiing, faSkiingNordic, faSnowboarding, faSnowplow,
	faQuoteLeft, faQuoteRight, faSpinner,
} from '@fortawesome/free-solid-svg-icons';

const icons = { faAnkh, faBan, faCog, faFlag, faSpinner, faSkating, faSkiing, faSkiingNordic, faSnowboarding, faSnowplow };

export default {
	title: 'Components/FontAwesomeIcon',
	component: 'font-awesome-icon',
	parameters: {
		controls: { disable: false, exclude: ['swapOpacity', 'mask', 'transform'] },
		docs: { description: { component:
			'Modeled after the official [vue-fontawesome / FontAwesomeIcon](https://github.com/FortAwesome/vue-fontawesome#basic) component.',
		} },
	},
	argTypes: {
		icon: { options: Object.keys(icons), control: 'select', mapping: icons },
		size: {	options: ['xs', 'sm', '', 'lg', '1x', '2x', '3x', '4x', '5x', '6x', '7x', '8x', '9x', '10x'], control: 'select' },
		rotation: {	options: [90, 180, 270, null], control: 'inline-radio' },
		pull: { options: ['left', 'right', null], control: 'inline-radio' },
		flip: { options: ['horizontal', 'vertical', 'both', null], control: 'inline-radio' },
		transform: { control: 'text' },
		...omitArgTypes([
			'--fa-primary-color',
			'--fa-primary-opacity',
			'--fa-secondary-color',
			'--fa-secondary-opacity',
		]),
	},
};

const Template = ({ icon, size, flip, fixedWidth, rotation, pull, spin, pulse, inverse, swapOpacity, mask, transform }) => html`
	<font-awesome-icon
		style=${inverse ? 'background: #222222; padding: 4px; border-radius: 4px' : ''}
		.icon=${icon}
		.size=${size}
		.flip=${flip}
		.fixedWidth=${fixedWidth}
		.rotation=${rotation}
		.pull=${pull}
		.spin=${spin}
		.pulse=${pulse}
		.inverse=${inverse}
		.swapOpacity=${swapOpacity}
		.mask=${mask}
		.transform=${transform}
	></font-awesome-icon>
`;

export const Base = Template.bind({});
Base.args = { icon: 'faAnkh', size: '3x' };

export const WithText = ({ icon, size, pull, text }) => html`
	<div>
		<font-awesome-icon .icon=${icon} .size=${size} .pull=${pull}></font-awesome-icon>
		${text}
	</div>
`;
WithText.args = { ...Base.args, size: '', text: 'Hello there' };
WithText.argTypes = omitArgTypes(['rotation', 'flip', 'fixedWidth', 'pulse', 'inverse', 'spin']);

export const Sizes = ({ icon, fixedWidth }, { parameters: { argTypes } }) => html`
	${argTypes.size.options.map(size => html`
		<font-awesome-icon .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	`)}
`;
Sizes.args = { ...Base.args, fixedWidth: false };
Sizes.argTypes = omitArgTypes(['rotation', 'flip', 'pulse', 'pull', 'inverse', 'spin', 'size']);

export const FixedWidth = () => html`
	<section style="font-size: 2rem;">
		<div><font-awesome-icon .icon=${faSkating} fixed-width></font-awesome-icon> Skating</div>
		<div><font-awesome-icon .icon=${faSkiing} fixed-width></font-awesome-icon> Skiing</div>
		<div><font-awesome-icon .icon=${faSkiingNordic} fixed-width></font-awesome-icon> Nordic Skiing</div>
		<div><font-awesome-icon .icon=${faSnowboarding} fixed-width></font-awesome-icon> Snowboarding</div>
		<div><font-awesome-icon .icon=${faSnowplow} fixed-width></font-awesome-icon> Snowplow</div>
	</section>
	<style>
		div:nth-child(even) font-awesome-icon { background: dodgerblue; }
		div:nth-child(odd) font-awesome-icon { background: skyblue; }
	</style>
`;
FixedWidth.args = { ...Base.args, size: '' };
FixedWidth.parameters = { controls: { disable: true } };
FixedWidth.decorators = [withShadow];

export const RotationAndFlip = ({ icon, size, fixedWidth }) => html`
	<font-awesome-icon .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon rotation="90" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon rotation="180" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon rotation="270" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon flip="horizontal" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon flip="vertical" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
	<font-awesome-icon flip="both" .icon=${icon} .size=${size} .fixedWidth=${fixedWidth}></font-awesome-icon>
`;
RotationAndFlip.args = { ...Base.args, icon: faSkiing, size: '4x', fixedWidth: true };
RotationAndFlip.argTypes = omitArgTypes(['rotation', 'flip', 'pulse', 'pull', 'inverse', 'spin']);

export const SpinAndPulse = ({ icon, size }) => html`
	<font-awesome-icon .icon=${icon} .size=${size} fixed-width spin></font-awesome-icon>
	<font-awesome-icon .icon=${icon} .size=${size} fixed-width pulse></font-awesome-icon>
`;
SpinAndPulse.args = { icon: 'faSpinner', size: '4x' };
SpinAndPulse.argTypes = omitArgTypes(['rotation', 'flip', 'fixedWidth', 'pulse', 'pull', 'inverse', 'spin']);

export const Pull = ({ size }) => html`
	<p>
		<font-awesome-icon .icon=${faQuoteLeft} .size=${size} pull="left"></font-awesome-icon>
		Gatsby believed in the green light, the orgastic future that year by year recedes before us.
		It eluded us then, but that’s no matter — tomorrow we will run faster, stretch our arms further...
		And one fine morning — So we beat on, boats against the current, borne back ceaselessly into the past.
	</p>
	<p>
		<font-awesome-icon .icon=${faQuoteRight} .size=${size} pull="right"></font-awesome-icon>
		Gatsby believed in the green light, the orgastic future that year by year recedes before us.
		It eluded us then, but that’s no matter — tomorrow we will run faster, stretch our arms further...
		And one fine morning — So we beat on, boats against the current, borne back ceaselessly into the past.
	</p>
`;
Pull.args = { size: '3x' };
Pull.argTypes = omitArgTypes(['icon', 'rotation', 'flip', 'fixedWidth', 'pulse', 'pull', 'inverse', 'spin']);

export const Inverse =  ({ icon, size }) => html`
	<font-awesome-icon .icon=${icon} .size=${size} inverse></font-awesome-icon>
`;
Inverse.args = { ...Base.args, size: '3x' };
Inverse.parameters = { backgrounds: { default: 'dark' } };
Inverse.argTypes = omitArgTypes(['rotation', 'flip', 'fixedWidth', 'pulse', 'pull', 'inverse', 'spin']);

function omitArgTypes(types) {
	const control = { type: null };
	const table = { disable: true };
	return Object.fromEntries(types.map(type => [type, { control, table }]));
}
