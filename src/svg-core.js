let _findIconDefinition;
let _icon;
let _text;
let _parse;

export function use({ findIconDefinition, icon, text, parse }) {
	_findIconDefinition = findIconDefinition;
	_icon = icon;
	_text = text;
	_parse = parse;
}

export function resolve(icon) {
	if (!_findIconDefinition) { return icon; }
	if (typeof icon === 'string') {
		return _findIconDefinition({ iconName: icon });
	}
	if (icon && 2 <= icon.length) {
		return _findIconDefinition({ prefix: icon[0], iconName: icon[1] });
	}
	return icon;
}

export function iconToHtml(...args) {
	return _icon ? pick('html', _icon(...args)) : undefined;
}

export function textToHtml(...args) {
	return _text ? pick('html', _text(...args)) : undefined;
}

export function parseTransform(...args) {
	return _parse ? _parse.transform(...args) : undefined;
}

function pick(prop, object) {
	if (!object) { return; }
	return object[prop];
}
