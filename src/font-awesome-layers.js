import { applyStyleWith } from './apply-style-with.js';
import { definePropsOn } from './core.js';
import { Props as IconProps } from './font-awesome-icon.js';
import { updateElementClass } from './utils.js';

const applyStyle = applyStyleWith({ css: /* css */`
	:host {
		box-sizing: border-box;
		display: inline-block;
		line-height: 1; }

		:host([pull="left"]) {
			float: left;
			margin-right: .3em; }

		:host([pull="right"]) {
			float: right;
			margin-left: .3em; }

	::slotted(*) {
		bottom: 0;
		left: 0;
		margin: auto;
		position: absolute;
		right: 0;
		top: 0; }
` });

export const template = document.createElement('template');
template.innerHTML = /* html */`
<span class="fa-layers">
	<slot></slot>
</span>
`;

const Internal = {
	LAYERS: Symbol('layers'),
};

const updateLayersClass = updateElementClass(Internal.LAYERS);
const prefixLayersClass = prefix => updateLayersClass(value => prefix.concat(value));

export const Props = {
	size: { ...IconProps.size, observe: prefixLayersClass('fa-') },
	fixedWidth: { ...IconProps.fixedWidth, observe: updateLayersClass('fa-fw') },
	pull: IconProps.pull,
};

export class FontAwesomeLayers extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		applyStyle(this);
		this[Internal.LAYERS] = shadowRoot.querySelector('.fa-layers');
	}
}
definePropsOn(FontAwesomeLayers, Props);
