import { attributeAsProp, setAttribute } from 'test/utils';
import { FontAwesomeLayers, Props } from './font-awesome-layers';

const TAG = 'fa-layers';
customElements['define'](TAG, FontAwesomeLayers);

describe('size', () => {
	const examples = ['2x', '3x'];
	testAttributeAsDefault({ prop: 'size', examples });
	testAttributeNotSynced({ prop: 'size', examples });
});

describe('pull', () => {
	const examples = ['left', 'right'];
	testAttributeAsDefault({ prop: 'pull', examples });

	test('attribute updates prop', () => {
		const { host } = setup();
		host.pull = 'left';
		setAttribute(host, 'pull', 'right');
		Props.pull.connect(host, 'pull');
		expect(host.pull).toBe('right');
	});

	test('prop change updates attribute', () => {
		const host = document.createElement(TAG);
		Props.pull.observe(host, 'left');
		expect(host.getAttribute('pull')).toBe('left');
	});
});

function testAttributeAsDefault({ prop, attribute = prop, examples = [], example = examples[0] }) {
	const message = prop === attribute ?
		'attribute initializes undefined prop' :
		`attribute "${attribute}" initializes undefined prop`;
	test(message, () => {
		const { host } = setup();

		setAttribute(host, attribute, example);
		Props[prop].connect(host, prop);
		expect(host[prop]).toBe(example);
	});
}

/** Due to performance reasons attributes are only meant to initialize undefined props. */
function testAttributeNotSynced({ prop, attribute = prop, examples = [] }) {
	const [example1, example2] = examples;
	const message = prop === attribute ?
		'attribute is not synced with prop' :
		`attribute "${attribute}" is not synced with prop`;
	test(message, () => {
		const { host } = setup();

		attributeResetsPropOnConnect({ prop, attribute, examples });

		setAttribute(host, attribute, example1);
		if (Props[prop].observe) {
			Props[prop].observe(host, example2, example1);
			expect(attributeAsProp(host, attribute)).toBe(example1);
		}
	});
}

function attributeResetsPropOnConnect({ prop, attribute = prop, examples: [example1, example2] }) {
	const { host } = setup();

	setAttribute(host, attribute, example1);
	host[prop] = example2;
	Props[prop].connect(host, prop);
	expect(host[prop]).toBe(example1);
}

function setup() {
	const host = document.createElement(TAG);
	const layer = () => host.shadowRoot.querySelector('span');
	return { host, layer };
}

