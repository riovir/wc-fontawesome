import { attributeAsProp, setAttribute } from 'test/utils';
import { FontAwesomeLayersText, Props } from './font-awesome-layers-text';

const TAG = 'fa-text';
customElements['define'](TAG, FontAwesomeLayersText);

describe('value', () => {
	const examples = ['1', '2'];
	testAttributeAsDefault({ prop: 'value', examples });
	testAttributeNotSynced({ prop: 'value', examples });

	test('left undefined renders as empty string by default', () => {
		const { text, counter, textContent } = setup();
		expect(text()).toBeTruthy();
		expect(counter()).toBeFalsy();
		expect(textContent()).toBe('');
	});

	test('left undefined renders as 0 for counters', () => {
		const { host, text, counter, textContent } = setup();
		host.counter = true;
		expect(text()).toBeFalsy();
		expect(counter()).toBeTruthy();
		expect(textContent()).toBe('0');
	});
});

describe('transform', () => {
	const examples = ['rotate-30', 'rotate-45'];
	testAttributeAsDefault({ prop: 'transform', examples });
	testAttributeNotSynced({ prop: 'transform', examples });
});

describe('size', () => {
	const examples = ['2x', '3x'];
	testAttributeAsDefault({ prop: 'size', examples });
	testAttributeNotSynced({ prop: 'size', examples });
});

describe('fixedWidth', () => {
	const examples = [true, false];
	testAttributeAsDefault({ prop: 'fixedWidth', attribute: 'fixed-width', examples });
	testAttributeNotSynced({ prop: 'fixedWidth', attribute: 'fixed-width', examples });
});

describe('inverse', () => {
	const examples = [true, false];
	testAttributeAsDefault({ prop: 'inverse', examples });
	testAttributeNotSynced({ prop: 'inverse', examples });
});

function testAttributeAsDefault({ prop, attribute = prop, examples = [], example = examples[0] }) {
	const message = prop === attribute ?
		'attribute initializes undefined prop' :
		`attribute "${attribute}" initializes undefined prop`;
	test(message, () => {
		const { host } = setup();

		setAttribute(host, attribute, example);
		Props[prop].connect(host, prop);
		expect(host[prop]).toBe(example);
	});
}

/** Due to performance reasons attributes are only meant to initialize undefined props. */
function testAttributeNotSynced({ prop, attribute = prop, examples = [] }) {
	const [example1, example2] = examples;
	const message = prop === attribute ?
		'attribute is not synced with prop' :
		`attribute "${attribute}" is not synced with prop`;
	test(message, () => {
		const { host } = setup();

		attributeResetsPropOnConnect({ prop, attribute, examples });

		setAttribute(host, attribute, example1);
		if (Props[prop].observe) {
			Props[prop].observe(host, example2, example1);
			expect(attributeAsProp(host, attribute)).toBe(example1);
		}
	});
}

function attributeResetsPropOnConnect({ prop, attribute = prop, examples: [example1, example2] }) {
	const { host } = setup();

	setAttribute(host, attribute, example1);
	host[prop] = example2;
	Props[prop].connect(host, prop);
	expect(host[prop]).toBe(example1);
}

function setup() {
	const host = document.createElement(TAG);

	const parseTransform = jest.fn();
	host._parseTransform = parseTransform;
	const text = () => host.shadowRoot.querySelector('.fa-layers-text');
	const counter = () => host.shadowRoot.querySelector('.fa-layers-counter');
	const textContent = () => (text() || counter()).textContent;
	return { host, text, counter, textContent, parseTransform };
}
