import { html } from 'lit-html';
import { faCircle, faCheck, faComment, faLightbulb, faShip, faWater } from '@fortawesome/free-solid-svg-icons';
import { pick } from '@/utils';
import iconStories from './font-awesome-icon.stories';

export default {
	title: 'Components/FontAwesomeLayers',
	component: 'font-awesome-layers',
	subcomponents: {
		FontAwesomeIcon: 'font-awesome-icon',
		FontAwesomeLayersText: 'font-awesome-layers-text',
	},
	parameters: {
		controls: { disable: false },
		docs: { description: { component:
			// eslint-disable-next-line max-len
			'Modeled after the official [vue-fontawesome / FontAwesomeLayers](https://github.com/FortAwesome/vue-fontawesome#advanced) component.\n\n' +
			'_Note that `size` is an extra prop that the official component only supports via the `fa-[size]` class._',
		} },
	},
	argTypes: {
		...pick(['size', 'pull'], iconStories.argTypes),

	},
};

export const Base = ({ size, pull, fixedWidth }) => html`
	<font-awesome-layers .size=${size} .pull=${pull} .fixedWidth=${fixedWidth}>
		<font-awesome-icon .icon=${faCircle}></font-awesome-icon>
		<font-awesome-icon .icon=${faCheck} transform="shrink-8" inverse></font-awesome-icon>
	</font-awesome-layers>
`;
Base.args = { size: '3x', fixedWidth: true };

export const TwoLayers = ({
	size, pull, fixedWidth,
	mainIcon, mainColor, mainTransform,
	backIcon, backColor,
}) => html`
	<font-awesome-layers .size=${size} .pull=${pull} .fixedWidth=${fixedWidth}>
		<font-awesome-icon .icon=${backIcon} style="color: ${backColor};"></font-awesome-icon>
		<font-awesome-icon .icon=${mainIcon} .transform=${mainTransform} style="color: ${mainColor};"></font-awesome-icon>
	</font-awesome-layers>
`;
TwoLayers.args = {
	...Base.args,
	mainIcon: 'faCog', mainColor: 'white', mainTransform: 'up-0.5 shrink-8 ',
	backIcon: 'faComment',
};
const mainIcons = { faCheck, faWater, faShip, ...iconStories.argTypes.icon.mapping };
const bottomIcons = { faCircle, faComment, faLightbulb };
TwoLayers.argTypes = {
	mainIcon: { options: Object.keys(mainIcons), mapping: mainIcons, control: 'select' },
	mainColor: { control: 'color' },
	mainTransform: { control: 'text' },
	backIcon: { options: Object.keys(bottomIcons), mapping: bottomIcons, control: 'select' },
	backColor: { control: 'color' },
};

export const StatusIcon = ({
	size, pull, fixedWidth,
	mainIcon, mainColor,
	statusIcon, statusColor, statusTransform,
	backIcon, backColor, backTransform,
}) => html`
	<font-awesome-layers .size=${size} .pull=${pull} .fixedWidth=${fixedWidth}>
		<font-awesome-icon .icon=${mainIcon} style="color: ${mainColor};"></font-awesome-icon>
		<font-awesome-icon .icon=${backIcon} .transform=${backTransform} style="color: ${backColor};"></font-awesome-icon>
		<font-awesome-icon .icon=${statusIcon} .transform=${statusTransform} style="color: ${statusColor};"></font-awesome-icon>
	</font-awesome-layers>
`;
StatusIcon.args = {
	...Base.args,
	mainIcon: 'faWater', mainColor: '#05386b',
	backIcon: 'faCircle', backColor: '#5cdb95', backTransform: 'right-6 down-5 shrink-6',
	statusIcon: 'faCheck', statusColor: '#edf5e1', statusTransform: 'right-6 down-5 shrink-10',
};
StatusIcon.argTypes = {
	...TwoLayers.argTypes,
	mainTransform: { table: { disable: true } },
	backTransform: { control: 'text' },
	statusIcon: { options: Object.keys(mainIcons), mapping: mainIcons, control: 'select' },
	statusColor: { control: 'color' },
	statusTransform: { control: 'text' },
};

export const Pull = () => html`
	<p>
		<font-awesome-layers size="3x" pull="left">
			<font-awesome-icon .icon=${faComment} flip="horizontal"></font-awesome-icon>
			<font-awesome-icon .icon=${faLightbulb} transform="shrink-7 up-.5" style="color: #5cdb95;"></font-awesome-icon>
		</font-awesome-layers>
		<font-awesome-layers size="4x" pull="right">
			<font-awesome-icon .icon=${faComment}></font-awesome-icon>
			<font-awesome-icon .icon=${faShip} transform="shrink-11 left-2 up-2" inverse></font-awesome-icon>
			<font-awesome-icon .icon=${faWater} transform="shrink-11 left-1 down-2" style="color: #8ee4af;"></font-awesome-icon>
		</font-awesome-layers>
		Gatsby believed in the green light, the orgastic future that year by year recedes before us.
		It eluded us then, but that’s no matter — tomorrow we will run faster, stretch our arms further...
		And one fine morning — So we beat on, boats against the current, borne back ceaselessly into the past.
	</p>
`;
Pull.parameters = { controls: { disable: true } };
