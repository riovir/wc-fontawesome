export function updateElementClass(prop, name) {
	if (name === undefined) {
		return name => updateElementClass(prop, name);
	}
	const classOf = (name, value) => typeof name === 'function' ? name(strictEscape(value)) : name;
	return (host, value, lastValue) => {
		if (!lastValue && !value) { return; }
		host[prop].classList.remove(classOf(name, lastValue));
		if (!value) { return; }
		host[prop].classList.add(classOf(name, value));
	};
}

function strictEscape(className) {
	return className ? className.replace(/[^0-9a-zA-Z-_]/g, '') : '';
}

export function setHostAttribute(attribute) {
	return (host, value) => host.setAttribute(attribute, value);
}
