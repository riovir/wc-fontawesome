export { FontAwesomeIcon } from './font-awesome-icon.js';
export { FontAwesomeLayers } from './font-awesome-layers.js';
export { FontAwesomeLayersText } from './font-awesome-layers-text.js';
import { use } from './svg-core.js';
export { use };

/* istanbul ignore next */
use(window.FontAwesome || {});
