import { IconDefinition, Transform } from '@fortawesome/fontawesome-svg-core';

/**
 * @element font-awesome-icon
 *
 * @prop {string|array|object} [icon=''] - Icon definition. Can be one of the following: <br />
 * - string: name of registered icons with the default prefix <br />
 * - array: of 'prefix', 'name' for registered icons with non-default prefix <br />
 * - object: the icon definition directly without the need to register it
 * @prop {object|undefined} [resolvedIcon=undefined] - (Read only) The resolved icon definition.
 *
 * @prop {string} [size=''] - one of the 'xs', 'xm', 'lg', '1x', '2x', ... '10x' sizes
 * @prop {string} [rotation=''] - one of the '90', '180', '270' rotations. For more control, use the transform prop.
 * @prop {string} [flip=''] - one of the 'horizontal', 'vertical', 'both' flip options
 * @prop {string} [pull=''] - one of the 'left', 'right' options to float the element
 * @prop {boolean} [fixedWidth=false] - fixes the width. Commonly used for aligning lists of icons
 * @prop {boolean} [spin=false] - sets the icon spinning. Commonly used for loading indicators
 * @prop {boolean} [pulse=false] - alternative to spinning. Commonly used with the spinner icon
 *
 * @prop {boolean} [inverse=false] - inverts the icon color
 * @prop {boolean} [swapOpacity=false] - swaps duotone icon layer opacities
 *
 * @prop {string|array|object} [mask=''] - same as icon, to use its shape as a mask. <br />
 * Used together with transform for proper relative sizing.
 * @prop {object|undefined} [resolvedMask=undefined] - (Read only) The resolved mask's icon definition.
 * @prop {string|object} [transform=''] - Transformation. Can be one of the following: <br />
 * - string: to be parsed into the object form <br />
 * - object: the transform definition directly
 * @prop {object|undefined} [parsedTransform=undefined] - (Read only) Transformation parsed into object form.
 *
 * @cssprop [--fa-primary-color=currentColor] Duotone icons only: primary background color
 * @cssprop [--fa-secondary-color=currentColor] Duotone icons only: secondary background color
 * @cssprop [--fa-primary-opacity=1] Duotone icons only: primary opacity (unless swapOpacity used)
 * @cssprop [--fa-secondary-opacity=0.4] Duotone icons only: secondary opacity (unless swapOpacity used)
 * */
declare class FontAwesomeIcon extends HTMLElement {
	fixedWidth: boolean;
	flip: 'horizontal' | 'vertical' | 'both' | '' | null;
	icon: object | Array<string> | string;
	get resolvedIcon(): object | undefined;
	inverse: boolean;
	mask: object | Array<string> | string | null;
	get resolvedMask(): object | undefined;
	pull: 'right' | 'left' | null;
	pulse: boolean;
	rotation: 90 | 180 | 270 | '90' | '180' | '270' | null;
	swapOpacity: boolean;
	size: '' | 'lg' | 'xs' | 'sm' | '1x' | '2x' | '3x' | '4x' | '5x' | '6x' | '7x' | '8x' | '9x' | '10x' | null;
	spin: boolean;
	transform: object | string | null;
	get parsedTransform(): object | undefined;

	connectedCallback(): void;
	disconnectedCallback(): void;
}

/**
 * @element font-awesome-layers-text
 * @prop {string|number} [value=''] - value to be displayed
 * @prop {boolean} [counter=false] - set to turn it to a counter subtype
 * @prop {string} [size=''] - one of the 'xs', 'xm', 'lg', '1x', '2x', ... '10x' sizes
 * @prop {boolean} [fixedWidth=false] - fixes the width. Commonly used for aligning lists of icons
 * @prop {string} [position=''] - one of the 'top-left', 'top-right', 'bottom-left', 'bottom-right' positions
 * @prop {boolean} [inverse=false] - inverts the icon color
 * @prop {string|object} [transform=''] - Transformation. Can be one of the following: <br />
 * - string: to be parsed into the object form <br />
 * - object: the transform definition directly
 * @prop {object|undefined} [parsedTransform=undefined] - (Read only) Transformation parsed into object form.
 *
 * @cssprop [--fa-layers-counter-background=#ff253a] background color of the counter
 * */
declare class FontAwesomeLayersText extends HTMLElement {
	counter: boolean;
	fixedWidth: boolean;
	inverse: boolean;
	position: 'bottom-left' | 'bottom-right' | 'top-left' | 'top-right' | null;
	transform: object | string | null;
	get parsedTransform(): object | undefined;
	value: string | number;

	connectedCallback(): void;
	disconnectedCallback(): void;
}

/**
 * @element font-awesome-layers
 *
 * @prop {string} [size=''] - one of the 'xs', 'xm', 'lg', '1x', '2x', ... '10x' sizes
 * @prop {string} [pull=''] - one of the 'left', 'right' options to float the element
 * @prop {boolean} [fixedWidth=false] - fixes the width. Commonly used for aligning lists of icons.
 */
declare class FontAwesomeLayers extends HTMLElement {
	fixedWidth: boolean;
	size: '' | 'lg' | 'xs' | 'sm' | '1x' | '2x' | '3x' | '4x' | '5x' | '6x' | '7x' | '8x' | '9x' | '10x' | null;
	pull: 'right' | 'left' | null;

	connectedCallback(): void;
	disconnectedCallback(): void;
}
