import { css } from './generated/styles.js';

const canAdoptStylesheet = ('adoptedStyleSheets' in document);

const Stylesheet = ({ css }) => {
	try {
		const sheet = new CSSStyleSheet();
		sheet.replaceSync(css);
		return sheet;
	}
	catch { return; }
};

const StyleTemplate = ({ css }) => {
	const template = document.createElement('template');
	template.innerHTML = /* html */`
		<style>${css}</style>
	`;
	return template;
};

const styleSheet = canAdoptStylesheet ? Stylesheet({ css }) : null;
const styleTemplate = !styleSheet ? StyleTemplate({ css }) : null;

const applyConstructedStyleWith = ({ css }) => {
	const ownCss = Stylesheet({ css });
	return host => {
		host.shadowRoot.adoptedStyleSheets = [styleSheet, ownCss];
	};
};

const applyInlineStyleWith = ({ css }) => {
	const IS_APPLIED = Symbol('is-applied');
	const ownCss = StyleTemplate({ css });
	return host => {
		if (host[IS_APPLIED]) { return; }
		host[IS_APPLIED] = true;
		host.shadowRoot.appendChild(styleTemplate.content.cloneNode(true));
		host.shadowRoot.appendChild(ownCss.content.cloneNode(true));
	};
};

export const applyStyleWith = styleSheet ?
	applyConstructedStyleWith :
	applyInlineStyleWith;
