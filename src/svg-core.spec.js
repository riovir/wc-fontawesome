import { use, resolve, parseTransform } from './svg-core';

describe('on its own', () => {
	test('resolve returns the argument as is', () => {
		const icons = [{}, null, () => ({}), 'icon', ['fa', 'icon']];
		icons.forEach(expectReturnsArg(resolve));
	});

	test('parseTransform returns undefined', () => {
		expect(parseTransform('ignored')).not.toBeDefined();
	});
});

describe('using { findIconDefinition }', () => {
	test('delegates string argument', using({ mockIcon: 'test-icon' }, () => {
		expect(resolve('string icon name')).toBe('test-icon');
	}));

	test('delegates array argument', using({ mockIcon: 'test-icon' }, () => {
		expect(resolve(['prefix', 'icon name'])).toBe('test-icon');
	}));

	test('return other args as is', using({ mockIcon: 'test-icon' }, () => {
		const icons = [{}, null, () => ({})];
		icons.forEach(expectReturnsArg(resolve));
	}));
});

function using({ mockIcon, config }, spec) {
	return () => {
		const findIconDefinition = jest.fn().mockReturnValue(mockIcon);
		use({ findIconDefinition, config });
		try { spec(); }
		finally {
			use({ findIconDefinition: undefined, config: undefined });
		}
	};
}

function expectReturnsArg(fn) {
	return arg => expect(fn(arg)).toBe(arg);
}
