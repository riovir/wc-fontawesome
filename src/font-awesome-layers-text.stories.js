import { html } from 'lit-html';
import { faCalendar, faCertificate, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { pick } from '@/utils';
import iconStories from './font-awesome-icon.stories';

export default {
	title: 'Components/FontAwesomeLayersText',
	component: 'font-awesome-layers-text',
	subcomponents: {
		FontAwesomeIcon: 'font-awesome-icon',
		FontAwesomeLayersText: 'font-awesome-layers-text',
	},
	parameters: {
		controls: { disable: false },
		docs: { description: { component:
			// eslint-disable-next-line max-len
			'Modeled after the official [vue-fontawesome / FontAwesomeLayersText](https://github.com/FortAwesome/vue-fontawesome#advanced) component.',
		} },
	},
	argTypes: {
		...pick(['size', 'transform'], iconStories.argTypes),
		value: { control: 'text' },
	},
};

export const Base = ({ value, size, transform, inverse, position }) => html`
<font-awesome-layers fixed-width size="4x" style=${!inverse ? 'background: #222222; padding: 4px; border-radius: 4px' : ''}>
	<font-awesome-icon .icon=${faCalendar} .inverse=${!inverse}></font-awesome-icon>
	<font-awesome-layers-text
		.transform=${transform}
		.value=${value}
		.size=${size}
		style="font-weight:900"
		.inverse=${inverse}
		.position=${position}
	></font-awesome-layers-text>
</font-awesome-layers>
`;
Base.args = { value: '27', transform: 'down-2 shrink-8', inverse: true };
Base.parameters = { controls: { exclude: ['counter', 'position'] } };

export const Counter = ({ value, position }) => html`
<font-awesome-layers fixed-width size="4x"}>
	<font-awesome-icon .icon=${faEnvelope}></font-awesome-icon>
	<font-awesome-layers-text counter .value=${value} .position=${position}></font-awesome-layers-text>
</font-awesome-layers>
`;
Counter.args = { value: '3', position: null };
Counter.argTypes = {
	position: { options: ['top-left', 'top-right', 'bottom-left', 'bottom-right', null], control: 'inline-radio' },
	'--fa-layers-counter-background': { control: 'color' },
};
Counter.parameters = { controls: { exclude: ['counter', 'transform', 'size', 'inverse'] } };

export const Inverse = () => html`
<font-awesome-layers fixed-width size="4x">
	<font-awesome-icon .icon=${faCalendar} inverse></font-awesome-icon>
	<font-awesome-layers-text transform="down-2 shrink-8" value="27" style="font-weight:900"></font-awesome-layers-text>
</font-awesome-layers>
<font-awesome-layers fixed-width size="4x">
	<font-awesome-icon .icon=${faCertificate} inverse></font-awesome-icon>
	<font-awesome-layers-text transform="shrink-11.5 rotate--30" value="NEW" style="font-weight:900"></font-awesome-layers-text>
</font-awesome-layers>
<font-awesome-layers fixed-width size="4x">
	<font-awesome-icon .icon=${faEnvelope} inverse></font-awesome-icon>
	<font-awesome-layers-text counter value="1"></font-awesome-layers-text>
</font-awesome-layers>
`;
Inverse.parameters = {
	backgrounds: { default: 'dark' },
	controls: { disable: true },
};
