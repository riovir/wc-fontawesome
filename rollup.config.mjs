export default [
	{
		input: 'src/index.js',
		external: ['@fortawesome/fontawesome-svg-core'],
		output: [
			{ file: 'dist/index.mjs', format: 'es' },
			{ file: 'dist/index.cjs', format: 'cjs' },
		],
	},
	{
		input: 'src/index-standalone.js',
		output: [
			{ file: 'dist/index-standalone.mjs', format: 'es' },
			{ file: 'dist/index-standalone.cjs', format: 'cjs' },
		],
	},
];
