export function setAttribute(host, attribute, value) {
	if (value === false) {
		host.removeAttribute(attribute);
	}
	const effectiveValue = value === true ? '' : value;
	host.setAttribute(attribute, effectiveValue);
}

export function attributeAsProp(host, attribute) {
	const value = host.getAttribute(attribute);
	if (value === null) { return false; }
	if (value === '') { return true; }
	return value;
}
