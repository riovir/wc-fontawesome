import { resolve } from 'path';
import fs from 'fs';
import * as coreApi from '@fortawesome/fontawesome-svg-core';
import { faCoffee } from 'test/fixtures/icons';
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from 'src';
import { use } from 'src/svg-core';

customElements.define('font-awesome-icon', FontAwesomeIcon);
customElements.define('font-awesome-layers', FontAwesomeLayers);
customElements.define('font-awesome-layers-text', FontAwesomeLayersText);

test('has all components', async () => {
	const { json } = await setup();
	expect(json.tags.map(prop('name'))).toEqual(expect.arrayContaining([
		'font-awesome-icon',
		'font-awesome-layers',
		'font-awesome-layers-text',
	]));
});

testDefaultsForEveryProperty('font-awesome-icon');
testDefaultsForEveryProperty('font-awesome-layers');
testDefaultsForEveryProperty('font-awesome-layers-text');

function testDefaultsForEveryProperty(tag) {
	test(`${tag} has defaults for every property`, async () => {
		const { filterPropsOf } = await setup();
		const ambiguousProps = filterPropsOf(tag, propEq('default', undefined));
		expect(ambiguousProps).toEqual([]);
	});
}

testDefaultsMatchElement('font-awesome-icon', FontAwesomeIcon);
testDefaultsMatchElement('font-awesome-layers', FontAwesomeLayers);
testDefaultsMatchElement('font-awesome-layers-text', FontAwesomeLayersText);

function testDefaultsMatchElement(tag, Element) {
	test(`${tag} documented defaults match code`, async () => {
		const { filterPropsOf } = await setup();
		const element = new Element();
		const isMismatch = prop => defaultOf(prop) !== element[prop.name];
		const mismatchedProps = filterPropsOf(tag, isMismatch);
		expect(mismatchedProps).toEqual([]);
	});
}

testPropTypeCoercion('font-awesome-icon', FontAwesomeIcon);
testPropTypeCoercion('font-awesome-layers', FontAwesomeLayers);
testPropTypeCoercion('font-awesome-layers-text', FontAwesomeLayersText);

function testPropTypeCoercion(tag, Element) {
	const anArray = ['fas', 'coffee'];
	const anObject = faCoffee;
	const caseStringOrObject = [
		{ given: 'coffee', expected: 'coffee' },
		{ given: 0, expected: '0' },
		{ given: 1, expected: '1' },
		{ given: null, expected: null },
		{ given: anArray, expected: anArray },
		{ given: anObject, expected: anObject },
	];
	const StringTypeCase = [
		{ given: 'v', expected: 'v' },
		{ given: 0, expected: '0' },
		{ given: 1, expected: '1' },
		{ given: null, expected: '' },
		{ given: [1], expected: String([1]) },
		{ given: { foo: 1 }, expected: String({ foo: 1 }) },
	];
	const TypeCases = {
		'string|array|object': caseStringOrObject,
		'string|object': caseStringOrObject,
		'boolean': [
			{ given: true, expected: true },
			{ given: 0, expected: false },
			{ given: 1, expected: true },
			{ given: '', expected: false },
			{ given: 'true', expected: true },
			{ given: 'false', expected: true }, // Yup, still a truthy value
			{ given: {}, expected: true },
			{ given: null, expected: false },
			{ given: undefined, expected: false },
		],
		'string|number': StringTypeCase,
		'string': StringTypeCase,
	};

	test(`${tag} ensures acceptable types of props`, async () => {
		const { propertiesOf } = await setup();
		const element = new Element();

		propertiesOf(tag).forEach(({ name: prop, type }) => {
			TypeCases[type].forEach(({ given, expected }) => {
				element[prop] = given;
				expect({ given, [prop]: element[prop] }).toEqual({ given, [prop]: expected });
			});
		});
	});
}

function defaultOf(prop) {
	if (prop.type.includes('string')) {
		return prop.default.replace(/[^\\]"/g, '');
	}
	if (prop.type.includes('boolean')) {
		return prop.default === 'true';
	}
}

/** Assuming custom-elements.json has already been generated */
async function setup() {
	const customElementsJsonPath = resolve(__dirname, '../custom-elements.json');
	const text = await fs.promises.readFile(customElementsJsonPath, 'utf-8');
	const json = JSON.parse(text);
	const isProp = ({ description = '' }) => !description.includes('(Read only)');
	const propertiesOf = tag => json.tags.find(propEq('name', tag)).properties.filter(isProp);
	const filterPropsOf = (tag, predicate) => propertiesOf(tag)
			.filter(isProp)
			.filter(predicate)
			.map(prop('name'));
	use(coreApi);
	coreApi.library.add(faCoffee);
	return { json, propertiesOf, filterPropsOf };
}

function prop(name) {
	return object => object[name];
}

function propEq(prop, value) {
	return object => object[prop] === value;
}
