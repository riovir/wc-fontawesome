import * as coreApi from '@fortawesome/fontawesome-svg-core';
import { faCircle, faCoffee, faTea, viewBoxOf, pathOf } from 'test/fixtures/icons';
import { use } from 'src/svg-core';
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from 'src';

customElements.define('font-awesome-icon', FontAwesomeIcon);
customElements.define('font-awesome-layers', FontAwesomeLayers);
customElements.define('font-awesome-layers-text', FontAwesomeLayersText);
coreApi.library.add(faCircle, faCoffee, faTea);

describe('FontAwesomeIcon', () => {
	const html = /* html */`
		<font-awesome-icon icon="coffee" size="3x" fixed-width></font-awesome-icon>
	`;

	test('initializes props based on attributes', () => {
		const { host } = setup({ html });
		expect(host.size).toBe('3x');
		expect(host.fixedWidth).toBe(true);
	});

	test('sets FontAwesome classes internally', () => {
		const { svg } = setup({ html });
		expect(svg().classList.contains('fa-3x')).toBe(true);
		expect(svg().classList.contains('fa-fw')).toBe(true);
	});

	test('renders registered icon', () => {
		const { svg, path } = setup({ html });
		expect(svg().getAttribute('viewBox')).toBe(viewBoxOf(faCoffee));
		expect(path().getAttribute('d')).toBe(pathOf(faCoffee));
	});

	test('transforms icon', () => {
		const html = /* html */`
			<font-awesome-icon icon="coffee" size="3x" fixed-width transform="rotate-42"></font-awesome-icon>
		`;
		const { svg } = setup({ html });
		const content = svg().innerHTML;
		expect(content).toEqual(expect.stringContaining('rotate('));
		expect(content).toEqual(expect.stringContaining('42'));
	});

	test('masks icon', () => {
		const html = /* html */`
			<font-awesome-icon icon="coffee" mask="circle" transform="shrink-3 up-1" size="4x"></font-awesome-icon>
		`;
		const { svg } = setup({ html });
		const content = svg().innerHTML;
		expect(content).toEqual(expect.stringContaining('clipPath'));
		expect(content).toEqual(expect.stringContaining('clip-path'));
	});

	test('ignores transform and mask on blank icon', () => {
		const html = /* html */`
			<font-awesome-icon size="3x" fixed-width mask="circle" transform="rotate-42"></font-awesome-icon>
		`;
		const { svg } = setup({ html });
		const content = svg().outerHTML;
		expect(content).toEqual(expect.stringContaining('viewBox'));
		expect(content).toEqual(expect.stringContaining('0 0 1 1'));
	});

	test('keeps rendered elements if possible', () => {
		const html = /* html */`
			<font-awesome-icon icon="coffee" size="4x"></font-awesome-icon>
		`;
		const { host, svg, path } = setup({ html });
		svg().setAttribute('tagged-by-test', '');
		path().setAttribute('tagged-by-test', '');
		host.icon = 'circle';
		expect(svg().hasAttribute('tagged-by-test')).toBe(true);
		expect(path().hasAttribute('tagged-by-test')).toBe(true);
	});

	test('re-renders DOM when mask is set', () => {
		const html = /* html */`
			<font-awesome-icon icon="coffee" mask="circle" transform="shrink-3 up-1" size="4x"></font-awesome-icon>
		`;
		const { host, svg } = setup({ html });
		host.mask = 'circle';
		svg().setAttribute('tagged-by-test', '');
		host.icon = 'tea';
		expect(svg().hasAttribute('tagged-by-test')).toBe(false);
	});

	function setup({ html }) {
		use(coreApi);
		document.body.innerHTML = html;
		const host = document.querySelector('font-awesome-icon');
		const svg = () => host.shadowRoot.querySelector('svg');
		const path = () => host.shadowRoot.querySelector('path');
		return { host, svg, path };
	}
});

describe('FontAwesomeLayersText', () => {
	const html = /* html */`
		<font-awesome-layers fixed-width size="4x">
			<font-awesome-layers-text transform="shrink-11.5 rotate--30" value="NEW" inverse></font-awesome-layers-text>
			<font-awesome-layers-text counter value="1"></font-awesome-layers-text>
		</font-awesome-layers>
	`;

	test('transforms layer text', () => {
		const { children } = setup({ html });
		const content = children[0].shadowRoot.innerHTML;
		expect(content).toEqual(expect.stringContaining('rotate('));
		expect(content).toEqual(expect.stringContaining('-30'));
	});

	test('keeps rendered elements if possible', () => {
		const { children: [host] } = setup({ html });
		host.transform = { size: 8 };
		const internals = textOf(host);
		internals.setAttribute('tagged-by-test', '');
		host.value = 'UPDATED';
		expect(internals.hasAttribute('tagged-by-test')).toBe(true);
	});

	function setup({ html }) {
		use(coreApi);
		document.body.innerHTML = html;
		const host = document.querySelector('font-awesome-layers');
		const layer = () => host.shadowRoot.querySelector('.fa-layers');
		return { host, layer, children: host.children };
	}

	function textOf(component) {
		return component.shadowRoot.querySelector('.fa-layers-text');
	}
});
