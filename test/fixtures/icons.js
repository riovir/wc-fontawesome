export const faCoffee = {
	prefix: 'fas',
	iconName: 'coffee',
	icon: [
		11,
		11,
		[],
		'f001',
		'... 1',
	],
};

export const faTea = {
	prefix: 'fas',
	iconName: 'tea',
	icon: [
		15,
		15,
		[],
		'f004',
		'... 4',
	],
};

export const faCircle = {
	prefix: 'fas',
	iconName: 'circle',
	icon: [
		22,
		22,
		[],
		'f002',
		'... 2',
	],
};

export const faDuotone = {
	prefix: 'fad',
	iconName: 'example-1',
	icon: [
		8,
		8,
		[],
		'f003',
		['... D1', '...D2'],
	],
};

export const faOtherDuotone = {
	prefix: 'fad',
	iconName: 'example-2',
	icon: [
		42,
		42,
		[],
		'f005',
		['... D3', '...D4'],
	],
};


export function viewBoxOf({ icon }) {
	return `0 0 ${icon[0]} ${icon[1]}`;
}

export function pathOf({ icon }) {
	return icon[4];
}
