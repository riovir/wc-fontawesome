# From 0.0.9 to 0.1.0

Instead of importing the `@riovir/wc-fontawesome/src/define/...` files, import the component classes and register the ones desired.

```js
// When having @fortawesome/fontawesome-svg-core installed
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from `@riovir/wc-fontawesome`;
// Alternatively, when using the standalone distribution which doesn't support the icon library, transforms, and masks
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from `@riovir/wc-fontawesome/dist/index-standalone.js`;

customElements.define('font-awesome-icon', FontAwesomeIcon);
customElements.define('font-awesome-layers', FontAwesomeLayers);
customElements.define('font-awesome-layers-text', FontAwesomeLayersText);
```
