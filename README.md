# @riovir/wc-fontawesome [![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](https://riovir.gitlab.io/wc-fontawesome)

> Non-official webcomponents for `Font Awesome`.

The API and usage is based on [vue-fontawesome](https://github.com/FortAwesome/vue-fontawesome).

## Installation

```bash
# Install peer dependency
$ npm i --save @fortawesome/fontawesome-svg-core
# Install the components
$ npm i --save @riovir/wc-fontawesome
# Install a Font Awesome format icon pack
$ npm i --save @fortawesome/free-solid-svg-icons
```

## Features

### Register your components first

```js
import { FontAwesomeIcon } from '@riovir/wc-fontawesome'
customElements.define('font-awesome-icon', FontAwesomeIcon);
// Alternatively, use a shorter name
customElements.define('fa-icon', FontAwesomeIcon);
```

For examples see the [Storybook](https://riovir.gitlab.io/wc-fontawesome).
While the code samples use [lit-html](https://lit-html.polymer-project.org/guide/writing-templates) syntax, this package does not require it.

### Why not self-define by default?

In the event you need to share the page with other teams using different versions of this component, you'll need some sort of scoping. You can read more about the topic on [open-wc.org](https://open-wc.org/scoped-elements/#limitations).
