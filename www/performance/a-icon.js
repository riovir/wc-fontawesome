export const template = document.createElement('template');
template.innerHTML = /* html */`
<svg
	aria-hidden="true"
	role="img"
	focusable="false"
	xmlns="http://www.w3.org/2000/svg"
	viewBox=""
>
	<path fill="currentColor" d="" />
</svg>
`;

export class FontAwesomeIcon extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._svg = shadowRoot.querySelector('svg');
		this._path = shadowRoot.querySelector('path');
		this._icon = {};
		this._size = null;
	}

	connectedCallback() {
		this.size = this.getAttribute('size')
	}

	get icon() {
		return this._icon;
	}
	set icon(value) {
		if (this._icon === value) { return; }
		this._icon = value;
		updateIcon(this, value)
	}

	get size() {
		return this._size;
	}
	set size(value) {
		if (this._size === value) { return; }
		this._size = value;
		updateSize(this, value)
	}
}

export function updateIcon(host, def) {
	const icon = def.icon;
	host._svg.setAttribute('viewBox', `0 0 ${icon[0]} ${icon[1]}`);
	host._path.setAttribute('d', icon[4]);
}

export function updateSize(host, size) {
	console.log(size);
	host._svg.classList.remove(...host._svg.classList);
	if (!size) { return; }
	host._svg.classList.add(`fa-${size}`);
}

customElements.define('font-awesome-icon', FontAwesomeIcon);
