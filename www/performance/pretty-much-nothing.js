export const template = document.createElement('template');
template.innerHTML = 'X';

export class PrettyMuchNothing extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
	}
}

customElements.define('font-awesome-icon', PrettyMuchNothing);
