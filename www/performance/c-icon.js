export const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		display: inline-block; }

	svg {
		display: inline-block;
		font-size: inherit;
		height: 1em;
		overflow: visible;
		vertical-align: -.125em; }

	.fa-lg {
		font-size: 1.33333em;
		line-height: 0.75em;
		vertical-align: -.0667em; }

	.fa-xs {
		font-size: .75em; }

	.fa-sm {
		font-size: .875em; }

	.fa-1x {
		font-size: 1em; }

	.fa-2x {
		font-size: 2em; }

	.fa-3x {
		font-size: 3em; }

	.fa-4x {
		font-size: 4em; }

	.fa-5x {
		font-size: 5em; }

	.fa-6x {
		font-size: 6em; }

	.fa-7x {
		font-size: 7em; }

	.fa-8x {
		font-size: 8em; }

	.fa-9x {
		font-size: 9em; }

	.fa-10x {
		font-size: 10em; }
</style>
<svg
	aria-hidden="true"
	role="img"
	focusable="false"
	xmlns="http://www.w3.org/2000/svg"
	viewBox=""
>
	<path fill="currentColor" d="" />
</svg>
`;

export class FontAwesomeIcon extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._svg = shadowRoot.querySelector('svg');
		this._path = shadowRoot.querySelector('path');
		this._icon = {};
		this._size = null;
	}

	connectedCallback() {
		this.size = this.getAttribute('size')
	}

	get icon() {
		return this._icon;
	}
	set icon(value) {
		if (this._icon === value) { return; }
		this._icon = value;
		updateIcon(this, value)
	}

	get size() {
		return this._size;
	}
	set size(value) {
		if (this._size === value) { return; }
		this._size = value;
		updateSize(this, value)
	}
}

export function updateIcon(host, def) {
	const icon = def.icon;
	host._svg.setAttribute('viewBox', `0 0 ${icon[0]} ${icon[1]}`);
	host._path.setAttribute('d', icon[4]);
}

export function updateSize(host, size) {
	console.log(size);
	host._svg.classList.remove(...host._svg.classList);
	if (!size) { return; }
	host._svg.classList.add(`fa-${size}`);
}

customElements.define('font-awesome-icon', FontAwesomeIcon);
