export const template = document.createElement('template');
template.innerHTML = /* html */`
<svg
	aria-hidden="true"
	role="img"
	focusable="false"
	xmlns="http://www.w3.org/2000/svg"
	viewBox=""
>
	<path fill="currentColor" d="" />
</svg>
`;

export class FontAwesomeIcon extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._svg = shadowRoot.querySelector('svg');
		this._path = shadowRoot.querySelector('path');
		this._icon = {};
		this._size = null;
	}

	connectedCallback() {
		this.size = this.getAttribute('size')
	}

	get icon() {
		return this._icon;
	}
	set icon(value) {
		if (this._icon === value) { return; }
		this._icon = value;
		updateIcon(this, value)
	}

	get size() {
		return this._size;
	}
	set size(value) {
		if (this._size === value) { return; }
		this._size = value;
		updateSize(this, value)
	}

	get prop1() { return this._prop1; }
	set prop1(value) {
		if (this._prop1 === value) { return; }
		this._prop1 = value;
		updateSize(this, value)
	}

	get prop2() { return this._prop2; }
	set prop2(value) {
		if (this._prop2 === value) { return; }
		this._prop2 = value;
		updateSize(this, value)
	}

	get prop3() { return this._prop3; }
	set prop3(value) {
		if (this._prop3 === value) { return; }
		this._prop3 = value;
		updateSize(this, value)
	}

	get prop4() { return this._prop4; }
	set prop4(value) {
		if (this._prop4 === value) { return; }
		this._prop4 = value;
		updateSize(this, value)
	}

	get prop5() { return this._prop5; }
	set prop5(value) {
		if (this._prop5 === value) { return; }
		this._prop5 = value;
		updateSize(this, value)
	}

	get prop6() { return this._prop6; }
	set prop6(value) {
		if (this._prop6 === value) { return; }
		this._prop6 = value;
		updateSize(this, value)
	}

	get prop7() { return this._prop7; }
	set prop7(value) {
		if (this._prop7 === value) { return; }
		this._prop7 = value;
		updateSize(this, value)
	}

	get prop8() { return this._prop8; }
	set prop8(value) {
		if (this._prop8 === value) { return; }
		this._prop8 = value;
		updateSize(this, value)
	}

	get prop9() { return this._prop9; }
	set prop9(value) {
		if (this._prop9 === value) { return; }
		this._prop9 = value;
		updateSize(this, value)
	}

	get prop10() { return this._prop10; }
	set prop10(value) {
		if (this._prop10 === value) { return; }
		this._prop10 = value;
		updateSize(this, value)
	}

	get prop11() { return this._prop11; }
	set prop11(value) {
		if (this._prop11 === value) { return; }
		this._prop11 = value;
		updateSize(this, value)
	}

	get prop12() { return this._prop12; }
	set prop12(value) {
		if (this._prop12 === value) { return; }
		this._prop12 = value;
		updateSize(this, value)
	}

	get prop13() { return this._prop13; }
	set prop13(value) {
		if (this._prop13 === value) { return; }
		this._prop13 = value;
		updateSize(this, value)
	}

	get prop14() { return this._prop14; }
	set prop14(value) {
		if (this._prop14 === value) { return; }
		this._prop14 = value;
		updateSize(this, value)
	}

	get prop15() { return this._prop15; }
	set prop15(value) {
		if (this._prop15 === value) { return; }
		this._prop15 = value;
		updateSize(this, value)
	}

	get prop16() { return this._prop16; }
	set prop16(value) {
		if (this._prop16 === value) { return; }
		this._prop16 = value;
		updateSize(this, value)
	}
}

export function updateIcon(host, def) {
	const icon = def.icon;
	host._svg.setAttribute('viewBox', `0 0 ${icon[0]} ${icon[1]}`);
	host._path.setAttribute('d', icon[4]);
}

export function updateSize(host, size) {
	console.log(size);
	host._svg.classList.remove(...host._svg.classList);
	if (!size) { return; }
	host._svg.classList.add(`fa-${size}`);
}

customElements.define('font-awesome-icon', FontAwesomeIcon);
