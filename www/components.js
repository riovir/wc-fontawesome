import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '/src/index-standalone.js';

customElements.define('font-awesome-icon', FontAwesomeIcon);
customElements.define('font-awesome-layers', FontAwesomeLayers);
customElements.define('font-awesome-layers-text', FontAwesomeLayersText);
