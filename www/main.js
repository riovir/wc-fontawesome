
// import { FontAwesomeIcon } from 'https://cdn.pika.dev/@riovir/wc-fontawesome@^0';
// customElements.define('font-awesome-icon', FontAwesomeIcon);
// import 'https://cdn.jsdelivr.net/npm/@riovir/wc-fontawesome@0.0.2/src/define/index.js';
// import * as core from 'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-svg-core/index.es.js';
// import { fas } from 'https://cdn.jsdelivr.net/npm/@fortawesome/free-solid-svg-icons@5.13.0/index.es.js';
// import { use } from '/src/index-standalone.js';
// use(core);
import { fas } from '/@fortawesome/free-solid-svg-icons/index.mjs';
import { faDuotoneMock } from './duotone.icons.js';
import './components.js';

/** Considering style performance */
// import './performance/pretty-much-nothing.js'; // 8 msec / ~1000 (won't get simpler than this)
// import './performance/a-icon.js'; // 45-50 msec / ~1000 (baseline, no styles)
// import './performance/b-icon.js'; // 49-52 msec / ~1000 (constructed stylesheets with all styles; unofficial; Chromium-only)
// import './performance/c-icon.js'; // 58-60 msec / ~1000 (inline some styles)
// import './performance/d-icon.js'; // 58-64 msec / ~1000 (inline all styles)
// import './performance/e-icon.js'; // 48-52 msec / ~1000 (+16 inline props)
// import './performance/f-icon.js'; // 50-52 msec / ~1000 (+16 defined props)

const defs = [faDuotoneMock].concat(Object.values(fas));

console.log(`Rendering ${defs.length} icon definitions`);
console.time();
defs.forEach(renderIcon);
console.timeEnd();

function renderIcon(icon) {
	document.body.appendChild(Element('font-awesome-icon', { icon }));
}

function Element(tag, props = {}) {
	const Constructor = customElements.get(tag);
	return Object.assign(new Constructor(), props);
}
