const { mkdir, readFile, writeFile } = require('fs/promises');
const { resolve } = require('path');
const { dependencies } = require('../package-lock');

const pkg = '@fortawesome/fontawesome-svg-core';
const file = `${pkg}/styles.css`;
const { version } = dependencies[pkg];

extractStyles({
	src: resolve(__dirname, `../node_modules/${file}`),
	target: resolve(__dirname, '../src/generated'),
	file: 'styles.js',
	map: toJsModule({ file, version }),
});

function toJsModule({ file, version }) {
	return css => `/* Extracted from ${file} v${version} */
export const css = /* css */\`
${css}
\`;
`;
}

async function extractStyles({ src, target, file, map }) {
	const css = await readFile(src, 'utf-8');
	await mkdir(target, { recursive: true });
	return writeFile(resolve(target, file), map(css), 'utf-8');
}
