# Change Log
All notable changes to this project will be documented in this file.

> Before releasing 1.0.0 API breaking changes will increase `minor` version. Everything else is `patch`.
> Before releasing 0.1.0 the package is considered purely experimental.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.15] - 2025-01-13
### Fixed
- **FontAwesomeIcon** no longer is flagged by automated accessibility tooling when no role or aria-label has been set.

## [0.1.14] - 2024-06-04
### Fixed
- **FontAwesomeIcon** no longer gets announced as "blank" on Firefox + NVDA.

## [0.1.13] - 2024-02-29
### Fixed
- Add missing **connectedCallback** and **disconnectedCallback** type declarations.

## [0.1.12] - 2023-03-28
### Fixed
- Legacy main and module declarations are mapping to the correct files now.

## [0.1.11] - 2023-03-24
### Fixed
- Make exported file extensions explicity reflect their module type.

## [0.1.10] - 2023-02-07
### Added
- Missing type definitions for the **Node16** style exports declarations.

### Fixed
- The code is no longer minified, as that's an app level concern.

## [0.1.9] - 2022-06-28

### Added
- Constructible stylesheet support.

## [0.1.8] - 2022-03-18

### Added
- Type definitions for TypeScript.

## [0.1.7] - 2022-03-17

### Changed
- Expanded peer dependency range to include **@fortawesome/fontawesome-svg-core v6**
- Updated dependencies to the latest version

## [0.1.6] - 2021-04-26
### Fixed
- Removed `??` from code so that `Webpack 4` won't trip over it.

## [0.1.5] - 2021-04-16
### Fixed
- Attributes now properly set props when the element is connected
- Properties are now initialized once the constructor has been called. This allows them to be inspected for runtime type information. In practice Vue.js now correctly passes `boolean` props when the attribute is used
- Properties are now be coerced to the right types
- **FontAwesomeLayersText** removed unusable `pull` prop also absent from `vue-fontawesome`

## [0.1.4] - 2021-04-13
### Fixed
- **FontAwesomeIcon** now properly recalculates transform on icon change.

## [0.1.3] - 2020-05-19
### Fixed
- **FontAwesomeIcon** now recalculates the DOM elements when using a `mask`.

## [0.1.2] - 2020-05-19
### Fixed
- **FontAwesomeIcon** no longer throws error in `standalone` mode when string transform is used.
- **FontAwesomeIcon** no longer throws error when `transform`ing or `mask`ing a component without an `icon`.

## [0.1.1] - 2020-05-19
### Fixed
- **FontAwesomeIcon** in its blank state has a `0 0 1 1` `viewBox` to prevent growing too large.

## [0.1.0] - 2020-05-18

### Breaking change
- **src/define/\*\*** has been removed to prevent uncertainty about which variant is being self-defined.
In the future this feature may be re-added as bundlers begin to support the `exports` in `package.json` as described in the `Node 14` docs.
See [UPGRADE.md](./UPGRADE.md) for details.

### Added
- **index-standalone** initializes `FontAwesome Core API` from `window.FontAwesome` if present.

## [0.0.9] - 2020-05-14
### Fixed
- **FontAwesomeIcon** boolean props toggling classes didn't remove them.

## [0.0.8] - 2020-05-14
### Added
- **FontAwesomeLayersText** is now supported as well.

## [0.0.7] - 2020-05-12
### Added
- **FontAwesomeLayers** has now basic support including `size` and `pull`.

## [0.0.6] - 2020-05-11
### Added
- **Masking** is now [supported](https://fontawesome.com/how-to-use/on-the-web/styling/masking) via the `mask` prop and attribute. In standalone mode however they are ignored.

### Fixed
- **FontAwesomeIcon** was incorrectly re-rendering the internal DOM structure when a simple shape update would've ben enough.

## [0.0.5] - 2020-05-09
### Added
- **Power transforms** are now [supported](https://fontawesome.com/how-to-use/on-the-web/styling/power-transforms) via the `transform` prop and attribute. In standalone mode however they are ignored, as the package relies on `@fortawesome/fontawesome-svg-core` to do the heavy lifting.

## [0.0.4] - 2020-05-04
### Added
- **Duotone icons** are now experimentally [supported](https://fontawesome.com/how-to-use/on-the-web/styling/duotone-icons). However, overriding the `familyPrefix` via the exported `use({ config })` function may be pulled, as the `Shadow DOM` boundary likely interferes with the intended use of the class prefix. Likely an alternative [Shadow parts](https://www.w3.org/TR/css-shadow-parts-1/) will need to be used to control the duotone icons from the outside, where the default CSS variables (or the `color` prop) are not preferred.

## [0.0.3] - 2020-04-28
### Added
- **ESLint** for cleaner code style
- **Jest** for dealing with the absent automated tests

## [0.0.2] - 2020-04-24
### Fixed
- **src/define/** files now only define the components without any awareness about the `@fortawesome/fontawesome-svg-core` package.

## [0.0.1] - 2020-04-24
- **Experimental release. See README for details**

