module.exports = {
	testEnvironmentOptions: { url: 'http://localhost/' },
	testEnvironment: 'jsdom',
	moduleNameMapper: {
		'^src(.*)$': '<rootDir>/src/$1',
		'^test(.*)$': '<rootDir>/test/$1',
	},
	collectCoverageFrom: [
		'src/**/*.js',
		'!src/**/*.stories.js',
	],
};
