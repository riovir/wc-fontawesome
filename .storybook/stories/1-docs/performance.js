import { FontAwesomeIcon } from 'src';

export function EveryIcon(pack) {
	const defs = Object.values(pack);
	const parent = document.createElement('div');
	const { icons, limit, log, refresh } = renderUi({ parent });

	const renderAll = renderWith(appendIcon);
	limit.addEventListener('keydown', ({ key }) => {
		if (key !== 'Enter') { return; }
		setTimeout(() => renderAll({ parent: icons, defs, log, limit: parseInt(limit.value) }), 1);
	});
	refresh.addEventListener('click', () => {
		setTimeout(() => renderAll({ parent: icons, defs, log, limit: parseInt(limit.value) }), 1);
	});
	setTimeout(() => renderAll({ parent: icons, defs, log }), 100);
	return parent;
}

function renderUi({ parent }) {
	const label = document.createElement('label');
	label.style.marginRight = '0.5rem';
	label.setAttribute('for', 'limit');
	label.innerHTML = 'Limit';
	parent.appendChild(label);

	const limit = document.createElement('input');
	limit.setAttribute('id', 'limit');
	limit.setAttribute('type', 'number');
	limit.setAttribute('step', '10');
	limit.style.marginRight = '0.5rem';
	parent.appendChild(limit);

	const refresh = document.createElement('button');
	refresh.innerHTML = 'Again';
	parent.appendChild(refresh);

	const log = document.createElement('h3');
	parent.appendChild(log);

	const icons = document.createElement('div');
	icons.style.display = 'flex';
	icons.style.flexWrap = 'wrap';
	icons.style.gap = '16px';
	parent.appendChild(icons);

	return { icons, limit, log, refresh };
}

function appendIcon(parent, icon) {
	const faIcon = new FontAwesomeIcon();
	faIcon.icon = icon;
	faIcon.size = '2x';
	parent.appendChild(faIcon);
}

function renderWith(appendIcon) {
	return ({ parent, defs, log, limit }) => {
		log.innerHTML = 'Rendering...';
		parent.innerHTML = '';
		const _defs = limit ? defs.slice(0, limit) : defs;

		const time = now();
		for (const icon of _defs) {
			appendIcon(parent, icon);
		}
		const duration = now() - time;
		log.innerHTML = `Rendering ${_defs.length} icons took ${duration}ms`;
	};
}

function now() {
	return Math.ceil(performance.now());
}
