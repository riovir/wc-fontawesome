/** As expected by Font Awesome v5.13.0
 * See: https://github.com/FortAwesome/Font-Awesome/blob/4e6402443679e0a9d12c7401ac8783ef4646657f/js-packages/%40fortawesome/fontawesome-svg-core/index.js#L1747-L1779
 */
export const faDuotoneMock = {
	prefix: 'fad',
	iconName: 'duotone-mock',
	icon: [
		512, // viewBox with of 512
		512, // viewBox height of 512
		[],
		'f999',
		[
			'M0 0L384 0L384 128L128 128L128 384L0 384L0 0', // top left L-shape
			'M512 512L128 512L128 384L384 384L384 128L512 128L512 512', // bottom right L-shape
		],
	],
};
