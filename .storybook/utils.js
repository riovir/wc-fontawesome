/** See: https://ramdajs.com/docs/#omit */
export function omit(props, object) {
	const result = { ...object };
	for (const prop of props) {
		delete result[prop];
	}
	return result;
}

/** See: https://ramdajs.com/docs/#pick */
export function pick(props, object) {
	const result = {};
	for (const prop of props) {
		result[prop] = object[prop];
	}
	return result;
}
