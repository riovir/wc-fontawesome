import { render, html } from 'lit-html';

const Bulma = html`
<link
	rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css"
	integrity="sha256-D9M5yrVDqFlla7nlELDaYZIpXfFWDytQtiV+TaH6F1I="
	crossorigin="anonymous"
>
<style>
:host * {
	box-sizing: border-box;
}
</style>`;

export function withHtml(Story) {
	const story = Story();
	return story.html || story;
}

export function withCssVariable(Story, { args }) {
	const { style } = document.documentElement;
	Object.entries(args)
			.filter(([argName, value]) => value && argName.startsWith('--'))
			.forEach(([argName, value]) => style.setProperty(argName, value));
	return Story();
}

export function withShadow(Story) {
	return renderShadow(Story());
}

export function withBulma(Story) {
	return renderShadow(html`${Bulma}${Story()}`);
}

export function withButFirst(Story) {
	const { butFirst, ...rest } = Story();
	butFirst();
	return rest;
}

function renderShadow(Html) {
	const div = document.createElement('div');
	const shadowRoot = div.attachShadow({ mode: 'open' });
	render(Html, shadowRoot);
	return div;
}
