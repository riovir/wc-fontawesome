module.exports = {
	stories: [
		'./**/*-docs/*.stories.@(js|mdx)',

		'../src/**/*-icon.stories.@(js|mdx)',
		'../src/**/*-layers.stories.@(js|mdx)',
		'../src/**/*-layers-text.stories.@(js|mdx)',
		'../src/**/*.stories.@(js|mdx)',

		'./**/*.stories.@(js|mdx)',
	],
	addons: [
		'@storybook/addon-essentials',
		'@storybook/addon-a11y',
	],
	core: { builder: 'webpack5' },
};
