import { setCustomElements } from '@storybook/web-components';
import { withHtml, withCssVariable } from '@/decorators';
import ce from '../custom-elements';
import './define-components';

setCustomElements(ce);

export const decorators = [withHtml, withCssVariable];
export const parameters = {
	actions: { disable: true },
	docs: { source: { type: 'code' }, transformSource },
	controls: { disable: true, hideNoControlsWarning: true },
	previewTabs: { canvas: { hidden: false } },
};

function transformSource(source) {
	const [/* ignore end */, ...rest] = source.split('\n').reverse();
	const [/* ignore start */, ...code] = rest.reverse();
	const [firstLine, ...otherLines] = code;
	const shiftedLines = otherLines.map(line => line.replace('\t', ''));
	return [firstLine, ...shiftedLines].join('\n');
}
