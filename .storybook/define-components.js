import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '../src';

customElements.define('font-awesome-icon', FontAwesomeIcon);
customElements.define('font-awesome-layers', FontAwesomeLayers);
customElements.define('font-awesome-layers-text', FontAwesomeLayersText);
