const { resolve } = require('path');

module.exports = async ({ config }) => {
	config.resolve.alias = {
		...config.resolve.alias,
		'src': resolve(__dirname, '../src'),
		'@': __dirname,
	};
	return config;
};
